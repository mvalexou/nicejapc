#!/acc/local/share/python/acc-py/pro/bin/python
import sys
sys.path.append('..')

import pyjapc
import nicejapc

# PyJAPC handle used by nicejapc
japc = pyjapc.PyJapc(incaAcceleratorName=None)

# Parameter time_resolution determines how often nicejapc.Timing checks for an
# event occurrence. Lower value increases accuracy, but also take more CPU
# time.
timing = nicejapc.Timing(japc, 'SPS', time_resolution=0.1)
event = 'SX.SCY-CT'

while True:
    # Wait for a specific event, then display the current user
    user = timing.wait(event)
    print('{0}: user = {1}'.format(event, user))
