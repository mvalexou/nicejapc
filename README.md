# nicejapc

Python wrapper for pyjapc, offering a more user friendly interface for FESA devices. For example:

```
japc = pyjapc.PyJapc(incaAcceleratorName=None)              # PyJAPC handle, used for communication
ctim = nicejapc.Device(japc, 'SX.SCY-CTML')                 # FESA device handle, for accessing properties
property_data = ctim.Acquisition.get('SPS.USER.SFTPRO1')    # multiplexed property get
item_data = ctim.Acquisition.acqC.get('SPS.USER.SFTPRO1')   # multiplexed value-item get
```

Note that scripts written using this module is expected to be executed on a CO virtual machine (not the frontend).

Timing class relies on CTIM subscriptions, so do not treat it as an accurate & precise timing source.
For reduced jitter and faster response, there is [pytimdt](https://gitlab.cern.ch/msuminsk/pytimdt) library, but it requires a timing receiver.

See [examples](../master/examples) directory for ready-to-run scripts.