import pyjapc
import requests
import json
import time

### FESA

class ValueItem:
    def __init__(self, prop, item_name):
        self._property = prop
        self._item_name = item_name

    def get(self, user=None):
        return self._property.get(user)[self._item_name]

    def set(self, value, user=None):
        data = self._property.get(user)
        data[self._item_name] = value
        self._property.set(data, user)

    def partial_set(self, value, user=None):
        data[self._item_name] = value
        self._property.set(data, user)


class Property:
    def __init__(self, device, property_name):
        self._device = device
        self._property_name = property_name

    def get(self, user=None):
        return self._device.get(self._property_name, user)

    def set(self, value, user=None):
        return self._device.set(self._property_name, value, user)

    def __getattr__(self, name):
        return ValueItem(self, name)


class Device:
    def __init__(self, japc, device_name, server_name=None):
        self._japc = japc
        self._device_name = device_name
        self._server_name = server_name
        self._default_user = '' # non-multiplexed properties

        # cache for data extracted from CCDE
        self._device_data = None
        self._class_data = None
        self._property_data = None

        if server_name:
            self._japc_url = "rda3://{0}/{1}".format(server_name, device_name)
        else:
            self._japc_url = device_name

    def set(self, property_name, value, override_user=None):
        unit_items = []
        user = override_user if override_user != None else self._default_user

        # data returned from PyJAPC getParam() is not always accepted by setParam()
        # this part adapts the data, so it is accepted by setParam():
        # - enums come as tuples, but setParam accepts only ints
        # - remove '*_units' value-items
        for k, v in value.items():
            if isinstance(v, tuple):
                value[k] = v[0]

            elif k.endswith('_units'):
                unit_items.append(k)

        for u in unit_items:
            value.pop(u, None)

        self._japc.setParam("{0}/{1}".format(self._japc_url, property_name),
                value, False, timingSelectorOverride=user)

    def get(self, property_name, override_user=None):
        user = override_user if override_user != None else self._default_user
        return self._japc.getParam("{0}/{1}".format(self._japc_url, property_name),
                False, timingSelectorOverride=user)

    def set_default_user(self, user):
        self._default_user = user

    def device_data(self):
        if not self._device_data:
            device_get_url = "https://ccda.cern.ch:8900/api/devices/{0}"

            s = requests.session()
            request = s.get(device_get_url.format(self._device_name), verify=False)
            self._device_data = json.loads(request.text)

        return self._device_data

    def class_data(self):
        if not self._class_data:
            class_get_url = "https://ccda.cern.ch:8900/api/deviceClasses/{0}/versions/{1}"
            class_name = self.device_data()['deviceClassInfo']['name']
            class_version = self.device_data()['deviceClassInfo']['version']

            s = requests.session()
            request = s.get(class_get_url.format(class_name, class_version), verify=False)
            self._class_data = json.loads(request.text)

        return self._class_data

    def property_data(self):
        if not self._property_data:
            self._property_data = self.class_data()['deviceClassProperties']

        return self._property_data

    def __getattr__(self, name):
        return Property(self, name)


### Timing

class Timing:
    def _subscription_cb(self, parameterName, val, header):
        if header['isFirstUpdate']:
            return

        event = parameterName.replace('ML/Acquisition', '')
        self._users[event] = header['selector']

    def __init__(self, japc, domain, time_resolution=0.1):
        self._japc = japc
        self._domain = domain
        self._time_resolution = time_resolution
        self._users = {}

    def __del__(self):
        self._japc.clearSubscriptions()

    # TODO perhaps there is a better way to implement this, without time.sleep?
    def wait(self, event):
        init_user = self._get_user(event)
        self._japc.subscribeParam(parameterName='{0}ML/Acquisition'.format(event),
                onValueReceived=self._subscription_cb,
                timingSelectorOverride='{0}.USER.ALL'.format(self._domain),
                getHeader=True)
        self._japc.startSubscriptions()

        user = None
        while True:
            user = self._get_user(event)

            if user != init_user:
                break

            time.sleep(self._time_resolution)

        self._japc.stopSubscriptions()
        return user

    def _get_user(self, event):
        if not event in self._users:
            self._users[event] = ''

        return self._users[event]
